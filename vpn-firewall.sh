#!/bin/bash

##Block SMTP,POP,SMB ports

/sbin/iptables -A FORWARD -p tcp -m multiport --dports 25,2525,465,587,26,110,995,143,993,139,445 -j DROP
/sbin/iptables -A FORWARD -p udp -m multiport --dports 25,2525,465,587,26,110,995,143,993,139,445 -j DROP
/sbin/iptables -A OUTPUT -p tcp -m multiport --dports 25,2525,465,587,26,110,995,143,993,139,445 -j DROP
/sbin/iptables -A OUTPUT -p udp -m multiport --dports 25,2525,465,587,26,110,995,143,993,139,445 -j DROP
/bin/sed -i 's/exit 0//g' /etc/rc.local
echo "/sbin/iptables -A FORWARD -p tcp -m multiport --dports 25,2525,465,587,26,110,995,143,993,139,445 -j DROP" >> /etc/rc.local
echo "/sbin/iptables -A FORWARD -p udp -m multiport --dports 25,2525,465,587,26,110,995,143,993,139,445 -j DROP" >> /etc/rc.local
echo "/sbin/iptables -A OUTPUT -p tcp -m multiport --dports 25,2525,465,587,26,110,995,143,993,139,445 -j DROP" >> /etc/rc.local
echo "/sbin/iptables -A OUTPUT -p udp -m multiport --dports 25,2525,465,587,26,110,995,143,993,139,445 -j DROP" >> /etc/rc.local
echo "exit 0" >> /etc/rc.local
echo "" >> /etc/rc.local
